import { Row } from "react-bootstrap";
import useDrink from "../hooks/useDrink"
import Drink from "./Drink";

const ListDrinks = () => {
    const { drinks } = useDrink();
  return (

        <Row className='mt-5'>
            {drinks.map(drink => (
                <Drink 
                    key={drink.idDrink}
                    drink={drink}
                />

            ))}
        </Row>
  )
}

export default ListDrinks
