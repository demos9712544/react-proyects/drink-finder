import { Col, Button, Card } from "react-bootstrap"
import useDrink from "../hooks/useDrink";

const Drink = ({ drink }) => {
  
  const { strDrink, idDrink, strDrinkThumb} = drink;

  const { handleModalClick, setDrinkId } = useDrink()

  return (
    <Col md={6} lg={3}>
      <Card className="mb-4">
        <Card.Img
          variant='top'
          src={strDrinkThumb}
          alt={`${strDrink} image`}
        />

        <Card.Body>
          <Card.Title>{ strDrink }</Card.Title>
          <Button
            onClick={ () => {
              handleModalClick()
              setDrinkId(idDrink)
            }}
            variant={"warning"}
            className="w-100"
          >{ strDrink }</Button>
        </Card.Body>
      </Card>
    </Col>
  )
}

export default Drink
