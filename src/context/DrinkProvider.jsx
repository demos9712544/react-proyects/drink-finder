import { useState, useEffect, createContext } from 'react'
import axios from 'axios'

const DrinkContext = createContext()

const DrinkProvider = ({children}) => {
    const [drinks, setDrinks] = useState([]);
    const [modal, setModal] = useState(false);
    const [drinkId, setDrinkId] = useState(null);
    const [recipe, setRecipe] = useState({});
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true)
        const getRecipe = async () => {
            if(!drinkId) return

            try {
                const url = `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${drinkId}`
            
                const { data } = await axios(url)
                setRecipe(data.drinks[0])
            } catch (error) {
                console.log(error)
            } finally {
                setLoading(false)
            }
        }
        getRecipe()
    }, [drinkId])

    const getDrink = async datos  => {
        try {
            const url = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${datos.name}&c=${datos.category}`

            const { data } = await axios(url)
            setDrinks( data.drinks )
        } catch (error) {
            console.log(error)
        }
    }

    //Modal
    const handleModalClick = () => {
        setModal(!modal)
    }

    //Search recipe
    const handleDrinkId = id => {
        setDrinkId(id)
    }    
    
    return (
        <DrinkContext.Provider
            value={{
                getDrink,
                drinks,
                handleModalClick,
                modal,
                handleDrinkId,
                recipe,
                setDrinkId,
                loading
            }}
        >
            {children}
        </DrinkContext.Provider>
    )
}

export {
    DrinkProvider
}

export default DrinkContext