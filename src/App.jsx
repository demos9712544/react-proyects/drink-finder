import { Container } from "react-bootstrap"
import Formulario from "./components/Formulario"
import { CategoriesProvider } from "./context/CategoriasProvider"
import { DrinkProvider } from "./context/DrinkProvider"
import ModalDrink from "./components/ModalDrink"
import ListDrinks from "./components/ListDrinks"

function App() {

  return (

      <CategoriesProvider>
        <DrinkProvider>
            <header className="py-5">
              <h1>Drink finder</h1>
            </header>

            <Container className="mt-5">
              <Formulario />

              <ListDrinks />
              <ModalDrink />
            </Container>


        </DrinkProvider>
      </CategoriesProvider>
  )
}

export default App
