import { useContext } from "react";
import CategoriesContext from "../context/CategoriasProvider";


const useCategories = () => {
    return useContext(CategoriesContext)
}

export default useCategories